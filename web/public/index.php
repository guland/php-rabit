<?php
session_start();

require_once '../app/bootstrap.php';

require __DIR__ . '/../vendor/autoload.php';


$app = new Gulacsi\Test\core\App();

// Adattáblák létrehozása
$migrate = new Gulacsi\Test\database\GenerateTables();
$migrate->users();
$migrate->advertisements();


// Táblák feltöltése kamu értékekkel
$seed = new Gulacsi\Test\database\SeedTables();


// Seed-elés
//if ($_ENV['DOCKERAPP_ENABLE_SEEDER'] === true) {
$seed->users();
$seed->advertisements();
//}



// Rekurzív mappanév-meghatározás mappaszintekkel
function rdirname($path, $count = 1)
{
  if ($count > 1) {
    return dirname(rdirname($path, --$count));
  } else {
    return dirname($path);
  }
}
