<?php

namespace Gulacsi\Test\controllers;

use Gulacsi\Test\core\Controller;


class Advertisement extends Controller
{

  /**
   * Az öszes reklám visszaadása
   * 
   * @param array $args
   * 
   * @return void
   */
  public function index()
  {
    $pageTitle = 'Advertisements';

    $ads = $this->db->query(
      "SELECT
        a.id AS id,
        a.user_id AS user_id,
        a.title AS title,
        u.name AS user_name
      FROM advertisements a
      INNER JOIN users u ON u.id = a.user_id"
    )->fetchAll();

    $this->view('adList', [
      'title' => $pageTitle,
      'ads' => $ads,
    ]);
  }
}
