<?php

namespace Gulacsi\Test\controllers;

use Gulacsi\Test\core\Controller;


class User extends Controller
{
  /**
   * Az összes felhasználó visszaadása
   * 
   * @param array $args
   * 
   * @return void
   */
  public function index()
  {
    $pageTitle = 'Users';
    $users = $this->db->query("SELECT * FROM users")->fetchAll();

    $this->view('userList', [
      'title' => $pageTitle,
      'users' => $users,
    ]);
  }
}
