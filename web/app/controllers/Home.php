<?php

namespace Gulacsi\Test\controllers;

use Gulacsi\Test\core\Controller;


class Home extends Controller
{

  /**
   * Főoldal
   * 
   * @return void
   */
  public function index()
  {
    $home = $this->model('Home');

    $home->title = 'Test exercise made by András Gulácsi';

    $this->view('home', [
      'title' => $home->title,
    ]);
  }
}
