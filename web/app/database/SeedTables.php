<?php

namespace Gulacsi\Test\database;

use Gulacsi\Test\services\PDOService;

require_once '../vendor/autoload.php';

use Dotenv\Dotenv as Dotenv;

// Adatbázis környezeti változók kiolvasása a .env fájlból
$dotenv = new Dotenv(rdirname(__FILE__, 1));
$dotenv->safeLoad();

class SeedTables
{
  // PDO wrapper
  protected $db;

  // A feltöltés letiltására
  public $seedDisabled = true;

  // Faker
  protected $faker;


  public function __construct()
  {
    $this->db = PDOService::instance();
    $this->faker = \Faker\Factory::create();
  }


  /**
   * Vásárlók tábla feltöltése kamu adatokkal
   * 
   * @return void
   */
  public function users()
  {
    if ($_ENV['DOCKERAPP_ENABLE_SEEDER'] === 'false') {
      return;
    }

    $max = 20;
    $data = [];

    for ($i = 0; $i < $max; $i++) {
      array_push($data, [
        $this->faker->name,
      ]);
    }

    $statement = $this->db->prepare("INSERT INTO users (name) VALUES (?)");
    try {
      $this->db->beginTransaction();
      foreach ($data as $row) {
        $statement->execute($row);
      }
      $this->db->commit();
    } catch (\Exception $e) {
      $this->db->rollback();
      throw $e;
    }
  }


  /**
   * Vásárlók tábla feltöltése kamu adatokkal
   * 
   * @return void
   */
  public function advertisements()
  {
    if ($_ENV['DOCKERAPP_ENABLE_SEEDER'] === 'false') {
      return;
    }

    $max = 30;
    $data = [];

    for ($i = 0; $i < $max; $i++) {
      array_push($data, [
        rand(1, 20),
        $this->faker->sentence,
      ]);
    }

    $statement = $this->db->prepare("INSERT INTO advertisements (user_id, title) VALUES (?,?)");
    try {
      $this->db->beginTransaction();
      foreach ($data as $row) {
        $statement->execute($row);
      }
      $this->db->commit();
    } catch (\Exception $e) {
      $this->db->rollback();
      throw $e;
    }
  }
}
