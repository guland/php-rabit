<?php

namespace Gulacsi\Test\database;

use Gulacsi\Test\services\PDOService;

class GenerateTables
{

  protected $db;

  public function __construct()
  {
    $this->db = PDOService::instance();
  }


  /**
   * Tábla létrehozása a user-eknek, ha még nem létezik
   * 
   * @return void
   */
  public function users()
  {

    $sql = "CREATE TABLE IF NOT EXISTS users(
    id INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (255) NOT NULL) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; ";

    $this->db->exec($sql);
  }


  /**
   * Tábla létrehozása a reklámoknak
   * 
   * @return void
   */
  public function advertisements()
  {
    $sql = "CREATE TABLE IF NOT EXISTS advertisements(
    id INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INT (11) NULL,
    title VARCHAR (255) NOT NULL,
    CONSTRAINT fk_user
      FOREIGN KEY (user_id)
      REFERENCES users (id)
      ON DELETE CASCADE) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; ";

    $this->db->exec($sql);
  }
}
