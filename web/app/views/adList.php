<?php extract($data); ?>

<html lang="hu">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $data['title'] ?></title>

  <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
  <link rel="stylesheet" href="/../../assets/style.css">
  <link rel="stylesheet" href="/../../node_modules/@fortawesome/fontawesome-free/css/all.min.css">

</head>

<body>
  <header>
    <div class="pure-menu pure-menu-horizontal">
      <a href="#" class="pure-menu-heading pure-menu-link">TESTAPP</a>
      <ul class="pure-menu-list">
        <li class="pure-menu-item">
          <a href="/public/" class="pure-menu-link">Home</a>
        </li>
        <li class="pure-menu-item">
          <a href="/public/user/index" class="pure-menu-link">Users</a>
        </li>
        <li class="pure-menu-item pure-menu-selected">
          <a href="/public/advertisement/index" class="pure-menu-link">Advertisements</a>
        </li>
      </ul>
    </div>
  </header>

  <div class="container">
    <h1><?php echo $data['title'] ?></h1>

    <ul class="user-grid">
      <?php
      foreach ($ads as $ad) {
        $li = '<li class="card">';
        $li .= '<h2 class="card-title">' . $ad['title'] . '</h2>';
        $li .= '<p class="gray-50"><i class="fas fa-user"></i>' . $ad['user_name'] . '</p>';
        $li .= '</li>';
        echo $li;
      }
      ?>
    </ul>
  </div>
</body>

</html>