# RabIT PHP tesztfeladat megoldása

## Instrukciók

1. Az .env fájlban van az összes konfiguráció (beleértve a jelszavakat). Nem muszáj változtatni az értékeken.
2. A composer csomagok telepítése szükséges (dotenv, faker): `composer install`
3. A /web mappában van a forrás. Szükséges a fontawesome ikonkészlet telepítése: `npm i`
4. A projekt belövése:

```
set -a
source .env
sudo docker-compose up --build
```

5. Az app elérhetősége: http://localhost:8000/public
6. Az adattáblák automatikusan létrejönnek, ha a fenti url betöltésre kerül. A seedelés is megvalósul. De először csak egyszer nyisd meg az url-t, mert a seeder folyamatosan töltené fel az adattáblákat. Az .env fájlban (`web/app/.env`) írd át utána: `DOCKERAPP_ENABLE_SEEDER=false`!

